# Bash scripting intro

**PRE-ALPHA, nothing to see here yet!**

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/engmark%2Fbash-scripting-intro/HEAD?labpath=intro.ipynb)

---

**The rest of this page is development documentation. Click the link above if
you want to learn Bash.**

## Use

Prerequisites: [Nix](https://nixos.org/download.html).

Run `nix-shell --run 'jupyter lab intro.ipynb'`.

## Support

Please check
[existing issues](https://gitlab.com/engmark/bash-scripting-intro/-/issues)
before
[filing a new one](https://gitlab.com/engmark/bash-scripting-intro/-/issues/new).

## Acknowledgments

Thank you to my employer, Toitū Te Whenua LINZ, for allowing me to spend time
developing this in the open and at work.

## License

[![License: CC BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
