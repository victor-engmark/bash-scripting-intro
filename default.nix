{ pkgs }:
pkgs.python3.withPackages (
  ps: [
    ps.bash_kernel
    ps.jupyterlab
  ]
)
