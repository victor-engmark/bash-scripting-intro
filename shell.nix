let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { };
  jupyter = import ./default.nix {
    inherit pkgs;
  };
in
pkgs.mkShell {
  buildInputs = [
    jupyter
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nixpkgs-fmt
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.statix
  ];
}
